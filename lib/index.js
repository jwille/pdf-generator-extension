'use strict'

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')
const fs = require('fs');
const fse = require('fs-extra');
const { exec } = require('child_process');

var logger

var headversion

module.exports.register = function ({ config }) {
    logger = this.require('@antora/logger').get(packageName)
    logger.info("registered")


    this.on('contentAggregated', function ({ contentAggregate }) {
        if (contentAggregate.length == 1) {
            headversion = contentAggregate[0].version
            logger.info("head version: " + headversion)
        }
    })

    this.on('contentClassified', async ({ contentCatalog }) => {

        logger.info("making pdf:")

        if (fs.existsSync('/temp')) {
            logger.info("deleting temp folder")
            fs.rmSync('/temp', { recursive: true, force: true })
        } else {
            logger.info("temp folder non existence")
        }




        fse.copySync('./modules/', './temp/modules/', { overwrite: true }, function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log("success!");
            }
        });

        fs.unlinkSync('./temp/modules/ROOT/pages/example$help');

        const sym = Object.getOwnPropertySymbols(contentCatalog).find(function (s) {
            return String(s) === "Symbol(files)";
        });

        contentCatalog.getFiles().forEach(_element => {
            var element

            if (_element.rel) {
                element = _element.rel
            } else {
                element = _element
            }

            var path = element.src.path
            var content = element._contents


            path = './temp/modules/ROOT/pages/example$help/' + element.src.basename

            logger.debug("path", path)

            const folder = './temp/modules/ROOT/pages/example$help/'
            logger.debug(folder)

            if (!fs.existsSync(folder)) {
                logger.debug("creating folder: '" + folder + "'")
                fs.mkdirSync(folder)
            }


            try {
                fs.writeFileSync(path, content);
                // file written successfully
            } catch (err) {
                console.error(err);
                throw new Error()
            }
        });

        const command = 'asciidoctor-pdf --failure-level ERROR --warnings temp/modules/ROOT/pages/felix-user-manual.adoc -o ' + config.data.output_folder

        logger.info("running asciidoctor")
        await execute_command(command)

        if (config.data.add_file_to_output) {
            logger.info("adding pdf to build")
            const pdf_data = fs.readFileSync(config.data.output_folder);
            try {
                addAssetsFile(contentCatalog, config.data.component, config.data.output_filename, pdf_data, config.data.version)
            } catch (error) {
                logger.info("File already existing")
                logger.debug(error)
            }
        }

        logger.info("conversion done! deleting temp folder")
        fs.rmSync('./temp', { recursive: true, force: true })

        logger.info("finished")
    })
}


function execute_command(command) {
    return new Promise(resolve => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
                // node couldn't execute the command
                console.error(err)
                throw new Error()
                return;
            }

            // the *entire* stdout and stderr (buffered)
            if(stderr){
                logger.warn(`Error with asciidoctor-pdf: ${stderr}`);
            }
            logger.info(`asciidoctor-pdf: ${stdout}`);
            
            resolve('finished')
        });
    })
}


function addAssetsFile(_contentCatalog, component, filename, data, version) {
    _contentCatalog.addFile({
        contents: data,
        path: 'modules/ROOT/attachments/' + filename,
        src: {
            path: 'modules/ROOT/attachments/' + filename,
            component: component,
            version: version,
            module: 'ROOT',
            family: 'attachment',
            relative: filename,
        },
    })
}
